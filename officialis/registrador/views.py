from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import viewsets, permissions
from .serializers import RegistroIPSerializer
from .models import RegistroIP

# Create your views here.
def index(request):
    return render(request, "registrador/index.html")

class RegistroIPViewSet(viewsets.ModelViewSet):
    queryset = RegistroIP.objects.all()
    serializer_class = RegistroIPSerializer

    def get_queryset(self):
        ip = self.request.query_params.get("ip")
        if ip:
            queryset = RegistroIP.objects.filter(ip=ip)
            return queryset
        else:
            return super().get_queryset()
