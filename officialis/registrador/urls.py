from django.urls import path, include
from . import views

from rest_framework import routers

router = routers.DefaultRouter()
router.register("", views.RegistroIPViewSet)


urlpatterns = [
    path("", views.index, name="index"),

    # API dos certificados
    path("registrador/api/", include(router.urls)),

]
