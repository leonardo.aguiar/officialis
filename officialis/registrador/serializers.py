from rest_framework import serializers
from .models import RegistroIP

class RegistroIPSerializer(serializers.ModelSerializer):

    class Meta:

        model = RegistroIP
        fields = ['usuario','descricao', 'mac_address', 'ip', 'data_registro']
