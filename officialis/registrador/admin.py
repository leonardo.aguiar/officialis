from django.contrib import admin

# Register your models here.
from django.contrib import admin

from .models import Vinculo, Unidade, Usuario, PoolIP, RegistroIP

admin.site.register(Vinculo)
admin.site.register(Unidade)
admin.site.register(Usuario)
admin.site.register(PoolIP)
admin.site.register(RegistroIP)
