from django.db import models

class Unidade(models.Model):
    nome = models.CharField("Unidade", max_length=255, unique=True)

    def __str__(self):
        return self.nome

class Vinculo(models.Model):
    nome = models.CharField("Tipo de vinculo", max_length=255 ,unique=True)

    def __str__(self):
        return self.nome

class Usuario(models.Model):
    identificacao = models.CharField(max_length=255, unique=True)
    unidade = models.ForeignKey(Unidade, on_delete=models.CASCADE)
    vinculo = models.ForeignKey(Vinculo, on_delete=models.CASCADE)

    def __str__(self):
        return self.identificacao

class PoolIP(models.Model):
    nome = models.CharField("Nome do Pool", max_length=255, unique=True)
    ip_prefixo = models.CharField("Prefixo de IP", max_length=15)
    netmask = models.CharField("Mascara de rede", max_length=15)
    gateway = models.CharField("Gateway", max_length=15)
    pool_inicio = models.IntegerField("Inicio", default=1)
    pool_final = models.IntegerField("Fim", default=254)

    def __str__(self):
        return self.nome+"("+self.ip_prefixo+" ["+str(self.pool_inicio)+"-"+str(self.pool_final)+"])"

class RegistroIP(models.Model):
    from django.utils import timezone
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    poolip = models.ForeignKey(PoolIP, on_delete=models.CASCADE)
    ip = models.CharField("Endereco IP", max_length=15, unique=True)
    mac_address = models.CharField("MAC Address", max_length=17, unique=True)
    descricao = models.CharField("Descricao", max_length=255, blank=True)
    data_registro = models.DateTimeField("Data de registro", default=timezone.now)
