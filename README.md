# Officialis
![Officialis](images/officialis_logo.png)

Repositório do Projeto Integrador III da Univesp, grupo 08.

**Membros**

- Alex Da Costa Silva
- Ana Paula Araujo Moreira
- Denis Bottura De Mello
- Leonardo De Conti Dias Aguiar
- Luan Felipe Pierro Almeida
- Nicholas Leal Giovannetti
- Thiago De Lourenço Manzutti
- Vitor Lopes Garcia



## Descrição do projeto

O Oficiallis é um sistema que está sendo desenvolvido para o autoatendimento do registro de MAC Address em serviços DHCP.